import fetch from 'node-fetch';

const REQUETE_MONDE = "https://api.covid19api.com/summary";
const PICTURE_MONDE = "https://clipground.com/images/world-history-clip-art-7.jpg";


class StatsMondeService {
    getDonnee() {
        return new Promise((resolve, reject) => {
            fetch(REQUETE_MONDE).then(httpResponse => {
                httpResponse.json().then(apiResponse => { 
                    resolve(apiResponse.Global);
                });
            });
        });
      }

      getPicture() {
        return new Promise((resolve, reject) => {
            fetch(PICTURE_MONDE).then(httpResponse => {
                httpResponse.json().then(apiResponse => { 
                    resolve(apiResponse);
                });
            });
        });
      }
}

export default StatsMondeService;