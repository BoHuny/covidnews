import fetch from 'node-fetch';

const REQUETE_PAYS = "https://api.covid19api.com/countries";
const REQUETE_STATS_PAYS = "https://api.covid19api.com/country"

class StatsPaysService {

    getListePays() {
        return new Promise((resolve, reject) => {
            fetch(REQUETE_PAYS).then(httpResponse => {
                httpResponse.json().then(apiResponse => {
                    resolve(apiResponse);
                });
            });
        });
    }

    getStatsPays(pays, dateDebut, dateFin) {
        let requete = REQUETE_STATS_PAYS + "/" + pays + "?from=" + dateDebut + "&to=" + dateFin;
        return new Promise((resolve, reject) => {
            fetch(requete).then(httpResponse => {
                httpResponse.json().then(apiResponse => {
                    resolve(apiResponse);
                });
            });
        });       
    }
}

export default StatsPaysService;

