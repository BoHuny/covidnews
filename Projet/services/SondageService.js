import fetch from 'node-fetch';

const REQUETE_STATS_SONDAGE = "http://covidsurvey.mit.edu:5000/query?"

class SondageService {
    getStatsSondage(age,genre,education,pays,signal) {
        let requete = REQUETE_STATS_SONDAGE + "age="+age+"&gender="+genre+"&education="+education+"&country="+pays+"&signal="+signal;
        return new Promise((resolve, reject) => {
            fetch(requete).then(httpResponse => {
                httpResponse.json().then(apiResponse => {
                    resolve(apiResponse);
                });
            });
        });       
    }
}

export default SondageService;

