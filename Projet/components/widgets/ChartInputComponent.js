import React, { Component } from 'react';
import { Dimensions, Text, View } from "react-native";

import { LineChart } from "react-native-chart-kit";

class ChartInputComponent extends Component {

  constructor(props) {
    super(props);
    this.state = {
        labels: [""],
        datasets: [{data:[0]}],
        legends: [""],
    };
  }

  setLabels(labels) {
      this.state.labels = labels;
  }

  setJeuDonnees(jeuDonnees) {
      let nData = {};
      nData.data = jeuDonnees.data;
      nData.color = (opacity = 1) => jeuDonnees.color;
      nData.strokeWidth = 2;
      nData.withDots = false;
      this.state.datasets = [nData];
      this.state.legends = [jeuDonnees.legend];
  }
  
  render() {
    return (
      <View>
        <LineChart
          data={{
            labels: this.state.labels,
            datasets: this.state.datasets,
            legend: this.state.legends
          }}
          width={Dimensions.get("window").width * 0.95}
          height={250}
          yAxisLabel=""
          yAxisSuffix=""
          yAxisInterval={100} // optional, defaults to 1
          chartConfig={{
            backgroundColor: "#e26a00",
            backgroundGradientFrom: "#fb8c00",
            backgroundGradientTo: "#ffa726",
            decimalPlaces: 0, // optional, defaults to 2dp
            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
            labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
            style: {
              borderRadius: 16
            },
            propsForDots: {
              r: "6",
              strokeWidth: "2",
              stroke: "#ffa726"
            }
          }}
          bezier
          style={{
            marginVertical: 8,
            borderRadius: 16,
            alignItems: 'center'
          }}
        />
      </View>
    );
  }

}

export default ChartInputComponent;
