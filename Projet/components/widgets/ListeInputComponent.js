import React, { Component } from 'react';
import RNPickerSelect from 'react-native-picker-select';

class ListeInputComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      value: null,
      items: this.props.items,
      placeholder: this.props.placeholder,
    };
  }

  setItems(items) {
    this.state.items = items;
  }

  getLabelDeValue(value) {
    for (let i = 0; i < this.state.items.length; i++) {
      if (this.state.items[i].value === value) {
        return this.state.items[i].label;
      }
    }
    return "";
  }

  render() {
    return (
      this.state.items.length > 0 && <RNPickerSelect
        useNativeAndroidPickerStyle={false}
        style=
        {
          {
            inputIOS: {
              fontSize: 16,
              paddingVertical: 12,
              paddingHorizontal: 10,
              borderWidth: 1,
              borderColor: 'black',
              borderRadius: 4,
              color: 'black',
            },
            inputAndroid: {
              fontSize: 16,
              paddingHorizontal: 10,
              paddingVertical: 8,
              borderWidth: 0.5,
              borderColor: 'black',
              borderRadius: 8,
              color: 'black',
            }
          } 
        }
        onValueChange={(value) => { this.state.value = value}}
        items={this.state.items}
        placeholder={{label:this.state.placeholder, value:null}}
        
      />  
    );
  }
}
export default ListeInputComponent;
