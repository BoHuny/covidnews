import React, { Component } from 'react';
import { TextInput } from 'react-native';

class TextInputComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      texte: "",
      placeholder: this.props.placeholder,
    };
    this.onChangementTexte = this.onChangementTexte.bind(this);
  }

  onChangementTexte(texte) {
      this.state.texte = texte;
  }

  render() {
    return (
      <TextInput
        placeholder={this.state.placeholder}
        onChangeText={this.onChangementTexte}
      />  
    );
  }
}
export default TextInputComponent;
