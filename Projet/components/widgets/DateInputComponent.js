import React, { Component, useState } from 'react';
import { View } from 'react-native';
import DatePicker from 'react-native-datepicker';

class DateInputComponent extends Component {

  constructor(props) {
    super(props);
    this.state = {
      date:this.getDateFormatee(props.date),
      compParent:props.compParent
    }

  }

  getDateFormatee(date) {
    return dateFormatee = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
  }

  render() {
    return (
      
      <View>
        <DatePicker
          style={{width: 200}}
          date={this.state.date}
          mode="date"
          placeholder="Select a date..."
          format="YYYY-MM-DD"
          minDate="2010-01-01"
          maxDate={this.getDateFormatee(new Date())}
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          customStyles={{
            dateIcon: {
              position: 'absolute',
              left: 0,
              top: 4,
              marginLeft: 0
            },
            dateInput: {
              marginLeft: 36
            }
          }}
          onDateChange={(date) => {this.state.date = date;this.state.compParent.forceUpdate()}}
        />
      </View>

    );
  }

}

export default DateInputComponent;
