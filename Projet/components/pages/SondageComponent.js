import React, { Component } from "react";
import {StyleSheet, View, Text, Button, Alert, ActivityIndicator } from "react-native";
import { FlatList } from "react-native-gesture-handler";
import { dataGroupAges, dataGender, dataEducation, dataCountries, dataSignal } from "../../resources/dataSondage";
import SondageService from "../../services/SondageService";
import ListeInputComponent from "../widgets/ListeInputComponent";

const PAS_DEMANDE = 0;
const ATTENTE = 1;
const CHARGE = 2;

class Sondage extends Component {
  constructor(props){
    super(props);
    this.inputs = {};
    this.request = this.request.bind(this);
    this.loadedView = this.loadedView.bind(this);
    this.renderItem = this.renderItem.bind(this);
    this.sondageService=new SondageService();
    this.state={
      etat:PAS_DEMANDE
    };
    this.data= [];
  }
  render() {
    return (
      <View style={styles.container} bounces={false}>
        <View style={styles.top} >
          <Text style={styles.title}>Survey info</Text>
        </View>
        <View style={styles.middle}>
            {this.Fields()}
            {this.state.etat===ATTENTE && this.loadingView()}
            {this.state.etat===CHARGE && this.loadedView()}
        </View>
      </View>
    );
  }
  Fields(){
    let affichage={}
    if (this.state.etat===CHARGE){
        affichage.display="none";
    }
    return (
      <View style={[styles.fields, affichage]}>
      {this.SelectBox("age","Age Group:",dataGroupAges) }
      {this.SelectBox("genre","Gender:",dataGender)}
      {this.SelectBox("education","Education:",dataEducation)}
      {this.SelectBox("pays","Country:",dataCountries)}
      {this.SelectBox("signal","Signal:",dataSignal)}
      <Button
        onPress={this.request}
        title="Confirm"
        color="#841584"
        disabled={this.attente}
        />
      </View>
    );
  }
  SelectBox (name, text, data){
    if (!this.inputs.hasOwnProperty(name)){
      let listeInputComponent = new ListeInputComponent({items: data.items,placeholder:data.placeholder});
      this.inputs[name]=listeInputComponent;
    }
    return (
      <View style={styles.boxContainer}>
        <Text style={styles.selectText}> {text} </Text>
        <View style = {{margin:10}}>
          {this.inputs[name].render()}
        </View>
      </View>
    );
  }
  loadingView(){
      return (
        <View>
          <ActivityIndicator animating={true} size="large" color="dodgerblue" />
        </View>
        );
  }
  loadedView(){
    let keys = Object.keys(this.data);
    let error = this.data.hasOwnProperty('error');
    if (error){
      Alert.alert('error',"Please broaden your search parameters");
      this.setEtat(PAS_DEMANDE);
    }
    else{
      let data = []
      keys.forEach(key =>data.push([key,this.data[key]]));
      this.data = data;
    }
    return(
        <View >
          <View>
          <Button
            onPress={()=>this.setEtat(PAS_DEMANDE)}
            title="Go back"
            color="#841584"
            disabled={false}
            />
          </View>
          <View style = {{height:"90%"}}>
          <FlatList 
            data = {this.data}
            renderItem = {this.renderItem}
            />
        </View>
        </View>      
    );

  }
  renderItem(item){
    let name = item.item[0];
    let value = item.item[1];
    return (
      <View style={styles.item}>
        <Text style={styles.itemText}>{name.replace(/_/g,' ')}</Text>
        {typeof(value)=='object' && <View >{this.processObject(value)}</View>}
        {typeof(value)!='object' && <Text style={{textAlign:'center'}}>{value}</Text>}
      </View>
    )    
  }
  processObject(param){
    let returnValue = [];
    let keys = Object.keys(param);
    keys.forEach(key=> {
      if (key!=='weighted'){
        if (param[key].hasOwnProperty('weighted')){
          console.log(key+" "+typeof(param[key]));
          let newParam = param[key]["weighted"];
          let newKeys = Object.keys(newParam);
          newKeys.forEach(key =>
            newParam[key]=(String(Math.round(newParam[key]*100))+" %"));
        }
        returnValue.push(
          <View style={styles.subItem}>
            <Text style ={{fontWeight:"bold",textAlign:"center"}}>{key.replace(/_/g, ' ')+":"}</Text>
            <Text style={{textAlign:"center"}}>
              {JSON.stringify(param[key],null,2).replace(/{/g, '').replace(/}/g, '')
              .replace(/_/g, ' ').replace(/,/g, '').replace(/weighted/g,'').replace(/"/g, '').replace(":","")}
            </Text>
          </View>
        );
      }
      else{
        let subKeys = Object.keys(param[key])
        subKeys.forEach(subKey =>
          param[key][subKey] = (String(Math.round(param[key][subKey]*100))+" %")
          )
        returnValue.push(
          <View style = {styles.subItem}>
          <Text style={{textAlign:"center"}}>
              {JSON.stringify(param[key],null,2).replace(/{/g, '').replace(/}/g, '')
              .replace(/_/g, ' ').replace(/,/g, '').replace(/weighted/g,'').replace(/"/g, '')}
            </Text>
            </View>
        )
      }      
    });
    return returnValue;
  }

  request(){
    let ageInput = this.inputs["age"].state.value;
    let genreInput = this.inputs["genre"].state.value;
    let edInput = this.inputs["education"].state.value;
    let paysInput = this.inputs["pays"].state.value;
    let signalInput = this.inputs["signal"].state.value;
    let ageVide =  ageInput=== null;
    let genreVide = genreInput === null;
    let edVide = edInput === null;
    let paysVide = paysInput === null;
    let signalVide = signalInput === null;
    let invalide = ageVide || genreVide || edVide || paysVide || signalVide;
    if (invalide){
      Alert.alert("One or more fields are empty");
    }
    else{
      this.setEtat(ATTENTE);
      this.sondageService.getStatsSondage(ageInput,genreInput,edInput,paysInput,signalInput).then((result)=>
        {
          this.data=result;
          this.setEtat(CHARGE);
        });
        
      
    }
  }
  setEtat(etat) {
    this.attente = (etat === ATTENTE);
    this.state.etat = etat;
    this.forceUpdate();
  }
  
}


const styles = StyleSheet.create({
  container:{
    backgroundColor:"#c8c7c7",
    flex:1
  },
  top:{
    backgroundColor: "#9ab3e5",
    justifyContent:"flex-end",
    height:"7.5%"
  },
  middle:{
    backgroundColor: "white", 
    padding: 20,
    margin: 10,
    borderRadius: 20,
    height:"90%"
  },
  title: {
    fontFamily:Platform.OS==='ios'  ? "AvenirNext-Heavy":"sans-serif",
    color: "white",
    fontSize: 25,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  boxContainer: {
    backgroundColor:"#6154a6",
    borderRadius: 30,
    justifyContent:"flex-start",
  },
  selectText:{
    textAlign: 'center',
    fontWeight: 'bold',
    color:"white"
  },
  fields:{
    backgroundColor: "white",
    justifyContent:"space-evenly",
    flex:1
  },
  item:{
    backgroundColor:"#1D4E89",
    justifyContent:"center",
    padding:10,
    margin:10,
    borderRadius:50


  },
  itemText:{
    color:"white",
    fontSize:20,
    textAlign:'center'
  },
  subItem:{
    padding:10,
    marginVertical:10,
    alignSelf:"center",
    backgroundColor:"#00B2CA",
    borderRadius:40
  }
});





export default Sondage;