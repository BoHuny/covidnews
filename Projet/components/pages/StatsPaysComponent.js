import React, { Component } from 'react';
import {StyleSheet, View, Text, Button, ScrollView, Alert, ActivityIndicator} from 'react-native';
import StatsPaysService from '../../services/StatsPaysService';
import TextInputComponent from '../widgets/TextInputComponent';
import DateInputComponent from '../widgets/DateInputComponent';
import ChartInputComponent from '../widgets/ChartInputComponent';

const PAGE_VIERGE = 0;
const ATTENTE_STATS = 1;
const AFFICHE_RESULTATS = 2;

class StatsPaysComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      etat:ATTENTE_STATS
    };

    this.statsPaysService = new StatsPaysService();
    this.recupererListePays();
    this.recupererStatsPays = this.recupererStatsPays.bind(this);
    let dateDebut = new Date();
    dateDebut.setMonth(dateDebut.getMonth() - 1);
    let dateFin = new Date();

    this.componentDateDebut = new DateInputComponent({date:dateDebut, compParent:this});
    this.componentDateFin = new DateInputComponent({date:dateFin, compParent:this});


    this.componentInputTexte = new TextInputComponent({placeholder: "Enter a country name..."});

    this.componentCharteCasConfirmes = new ChartInputComponent();
    this.componentCharteMorts = new ChartInputComponent();
    this.componentCharteGuerisons = new ChartInputComponent();
    this.componentBoutonValider = new Button(
      {
        onPress:this.recupererStatsPays,
        title:"Confirm",
        disabled:false
      }
    );
    this.pays = [];
    this.nomPaysStats = null;
  }

  recupererListePays() {
    this.statsPaysService.getListePays().then(listePays => {
      listePays.sort(function(a, b) {
        return a.Country.localeCompare(b.Country); 
      });
      for (let i = 0; i < listePays.length; i++) {
        let p = {};
        p.label = listePays[i].Country;
        p.value = listePays[i].Slug;
        this.pays.push(p);
      }

      this.setEtat(PAGE_VIERGE);
      this.forceUpdate();
    });
  }

  setLabelsChartes(labels) {
    this.componentCharteCasConfirmes.setLabels(labels);
    this.componentCharteMorts.setLabels(labels);
    this.componentCharteGuerisons.setLabels(labels);
  }

  setEtat(etat) {
    this.componentBoutonValider.props.disabled = (etat === ATTENTE_STATS);
    this.state.etat = etat;
    this.forceUpdate();
  }

  

  recupererStatsPays() {
    this.labelPays = this.componentInputTexte.state.texte;
    if (this.labelPays[this.labelPays.length - 1] === " ") { // Espace en fin de chaine si pays autocomplete
      this.labelPays = this.labelPays.slice(0, -1);
    }
    this.labelPays = this.labelPays.charAt(0).toUpperCase() + this.labelPays.slice(1).toLowerCase();
    let valeurPays = this.getValeurPaysDeLabel(this.labelPays);
    let dateDebutFormatee = this.componentDateDebut.state.date + "T00:00:00Z";
    let dateFinFormatee = this.componentDateFin.state.date + "T00:00:00Z";
    let tempsDateDebut = this.getDateDeChaine(this.componentDateDebut.state.date).getTime();
    let tempsDateFin = this.getDateDeChaine(this.componentDateFin.state.date).getTime();
    if (tempsDateDebut >= tempsDateFin) {
      Alert.alert(
        "Input error",
        "Please select a start date prior to the end date",
        [
          { text: "OK" }
        ]
      );
    }

    else if (valeurPays === null) {
      Alert.alert(
        "Input error",
        "Please enter a valid country",
        [
          { text: "OK" }
        ]
      );
    }

    else {
      this.setEtat(ATTENTE_STATS);
      let donneesConfirmes = {
        data:[],
        color:`rgba(0, 0, 255, 1)`,
        legend:"Confirmed cases"
      };    
      let donneesMorts = {
        data:[],
        color:`rgba(255, 0, 0, 1)`,
        legend:"Deaths"
      };
      let donneesGueris = {
        data:[],
        color:`rgba(0, 255, 0, 1)`,
        legend:"Recovered"
      };
  
      let duree = tempsDateFin - tempsDateDebut;
      let nombreDureesLabel = 3;
      let dureePas = duree / nombreDureesLabel;
      let labels = [];
      
      for (let i = 0; i < nombreDureesLabel; i++) {
        let tempsCourant = new Date(tempsDateDebut + i * dureePas);
        let labelCourant = tempsCourant.getDate() + "/" + (tempsCourant.getMonth() + 1) + "/" + tempsCourant.getFullYear();
        labels.push(labelCourant);
      }
  
      this.statsPaysService.getStatsPays(valeurPays, dateDebutFormatee, dateFinFormatee).then(
        stats => {
          if (stats.hasOwnProperty("message")) {
            Alert.alert("Error processing the request", "For perfomance reasons, the API can only provide information for an interval up to a week. Please modify the interval in consequences before resending the request.");
            this.setEtat(PAGE_VIERGE);
          }
          else {
            stats.forEach((stat) => {
              if (stat.Province === "" && stat.City === "") {
                donneesConfirmes.data.push(stat.Confirmed);
                donneesMorts.data.push(stat.Deaths);
                donneesGueris.data.push(stat.Recovered);
              }
            });
            this.setLabelsChartes(labels);
            if (donneesConfirmes.data.length === 0) {
              donneesConfirmes.data.push(0);
            }
            if (donneesMorts.data.length === 0) {
              donneesMorts.data.push(0);
            }
            if (donneesGueris.data.length === 0) {
              donneesGueris.data.push(0);
            }
            this.componentCharteCasConfirmes.setJeuDonnees(donneesConfirmes);
            this.componentCharteMorts.setJeuDonnees(donneesMorts);
            this.componentCharteGuerisons.setJeuDonnees(donneesGueris);
            this.setEtat(AFFICHE_RESULTATS);
          }
        }
      )
    }
  }

  getValeurPaysDeLabel(labelPays) {
    let labelLower = labelPays.toLowerCase();
    for (let i = 0; i < this.pays.length; i++) {
      if (this.pays[i].label.toLowerCase() === labelLower) {
        return this.pays[i].value;
      }
    }
    return null;   
  }

  getDateDeChaine(chaine) {
    let date = new Date();
    let partsDate = chaine.split("-");
    date.setFullYear(parseInt(partsDate[0]));
    date.setMonth(parseInt(partsDate[1] - 1));
    date.setDate(parseInt(partsDate[2]));
    return date;
  }

  render() {
    return (
      <ScrollView style={styles.container} bounces={false}>
        <View>
          <View style={styles.top}>
            <Text style={styles.title}>
              Statistics for a specific country
            </Text>
          </View>
          <View style={styles.middle}>
            {this.componentInputTexte.render()}
            <Text>From</Text>
            {this.componentDateDebut.render()}


            <Text>To</Text>
            {this.componentDateFin.render()}
            <View style={{"marginTop":20}}>
            {this.componentBoutonValider.render()}
            </View>
          </View>
        </View>

        {this.state.etat == ATTENTE_STATS && 
          <View style={{
            flex: 1,
            justifyContent: "center",
            flexDirection: "row",
            justifyContent: "space-around",
            padding: 10
          }}>
            <ActivityIndicator animating={true} size="large" color="00ff00" />
          </View>
        }
        {this.state.etat == AFFICHE_RESULTATS && 
          <View>
            <Text style={
              {
                fontSize:15,
                textAlign:"center"
              }
            }>
              Statistics for {this.labelPays}
              </Text>
            <View>
                { this.componentCharteCasConfirmes.render() }
                { this.componentCharteMorts.render() }
                { this.componentCharteGuerisons.render() }
            </View>
          </View>
        }
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    backgroundColor:"#c8c7c7",
    flex: 1
  },

  top:{
    paddingTop:30,
    backgroundColor: "#9ab3e5", 
    flex:0.2,
    justifyContent:"flex-end"
  },

  middle:{
    backgroundColor: "white", 
    flex: 1,
    padding: 20,
    margin: 10,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    justifyContent:'space-evenly'
  },

  title: {
    fontFamily:"AvenirNext-Heavy",
    color: "white",
    fontSize: 25,
    textAlign: 'center',
    fontWeight: 'bold',
  },

});


export default StatsPaysComponent;
