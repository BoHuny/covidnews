import React, { Component } from 'react';
import {StyleSheet, View, Text, Image, Button, SegmentedControlIOSComponent} from 'react-native';
import StatsMondeService from '../../services/StatsMondeService';

class StatsMondeComponent extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
        donnee1:null,
    }
    this.statsMondeService = new StatsMondeService();
    this.recupererDonnee();
  }

  recupererDonnee() {
    this.statsMondeService.getDonnee().then(donnee => {
        this.setState({
            donnee1: donnee
        });
    });
    }

    


  MaBox (text, data){
    return (
      <View style={styles.boxContainer}>
        <Text style={styles.selectText}> {text} </Text>
        <Text style={styles.myText}>{data}</Text>
      </View>
      );
    }


  MaBox1 (text, data){
      return (
        <View style={styles.ContainerDate} >
          <Text style={styles.selectText1}> {text} </Text>
          <Text style={styles.myText1}>{data}</Text>
        </View>
        );
  }

  render() {

    function numStr(a, b) {
      a = '' + a;
      b = b || ' ';
      var c = '',
          d = 0;
      while (a.match(/^0[0-9]/)) {
        a = a.substr(1);
      }
      for (var i = a.length-1; i >= 0; i--) {
        c = (d != 0 && d % 3 == 0) ? a[i] + b + c : a[i] + c;
        d++;
      }
      return c;
    }

    if(this.state.donnee1!=null ){
      let mystring = JSON.stringify(this.state.donnee1.Date)
      let mysub1= mystring.substring(1,11)
      let mysub2= mystring.substring(13,20)
      

      let New_Confirmed = numStr(this.state.donnee1.NewConfirmed)
      let Total_Confirmed =  numStr(this.state.donnee1.TotalConfirmed)
      let New_Deaths = numStr(this.state.donnee1.NewDeaths)
      let Total_Deaths = numStr(this.state.donnee1.TotalDeaths)
      let New_Recovered =  numStr(this.state.donnee1.NewRecovered)
      let Total_Recovered = numStr(this.state.donnee1.TotalRecovered)

      return (
        <View style={styles.container}>
          <View style={styles.top} >
            <Text style={styles.title}>Worldwide Covid News</Text>
          </View>
          <View style={styles.middle} >
            <View style={{flexDirection: "row", justifyContent:'space-between', flex:0.1}}>
                {this.MaBox1("Date:",mysub1)}
                {this.MaBox1("Hour:",mysub2)}
            </View>
            <Image
                      source={require('../../assets/IMAGE2.png')}
                      style={styles.card__image}
                  />
            {this.MaBox("New confirmed cases:",New_Confirmed)}
            {this.MaBox("Total of confirmed cases:",Total_Confirmed)} 
            {this.MaBox("New deaths:",New_Deaths)}
            {this.MaBox("Total of deaths:",Total_Deaths)}
            {this.MaBox("New recovered:",New_Recovered)}
            {this.MaBox("Total of recovered:",Total_Recovered)}
          </View>
        </View>
      )
    }
    else{
      return(
        <View style = {{justifyContent:"center"}}>
        <Text style = {{fontWeight: "bold", fontSize:40}}>Unable to fetch data</Text>
      </View>
      )

    }
  }
}



const styles = StyleSheet.create({
  container:{
    backgroundColor:"#c8c7c7",
    flex: 1
  },

  ContainerDate:{
    backgroundColor:"white",
    flex:1
  },

  container1:{
    backgroundColor:"white",
    flex: 0.1,
    flexDirection: "row",
  },

  top:{
    backgroundColor: "#9ab3e5", 
    height:"8%",
    justifyContent:"flex-end",
  },

  middle:{
    backgroundColor: "white", 
    flex: 1,
    padding: 20,
    margin: 10,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    justifyContent:'space-evenly',
  },

  title: {
    fontFamily:"AvenirNext-Heavy",
    color: "white",
    fontSize: 25,
    textAlign: 'center',
    fontWeight: 'bold',
  },

  boxContainer: {
    backgroundColor:"#6154a6",
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    justifyContent:"center",
    flex:0.09
  },



  selectText:{
    textAlign: 'center',
    fontWeight: 'bold',
    color:"white"
  },

  selectText1:{
    textAlign: 'center',
    fontWeight: 'bold',
    color:"black"
  },

  myText:{
    textAlign: 'center',
    fontWeight: 'bold',
    color:"orange",
    fontSize: 25
  },

  myText1:{
    textAlign: 'center',
    fontWeight: 'bold',
    color:"orange",
    fontSize: 25
  },

  card__image: {
    flex: 0.3,
    width:"65%",
    height: "65%",
    borderRadius: 16,
    marginRight: 10,
    alignSelf: 'center',
}
});


export default StatsMondeComponent;