const dataGroupAges = {
    items :[{label: "All", value: "all"},
            {label: "20-30", value: "20-30"},
            {label: "31-40", value: "31-40"},
            {label: "41-50", value: "41-50"},
            {label: "51-60", value: "51-60"},
            {label: "61-70", value: "61-70"},
            {label: "71-80", value: "71-80"}
    ],
    placeholder: "Please select the age group"
  };
  const dataGender = {
    items: [
      {label: "All", value: "all"},
      {label: "Male", value: "male"},
      {label: "Female", value: "female"},
      {label: "Other", value: "other"}
    ],
    placeholder: "Please select the gender"
  };
  
  const dataEducation = {
    items : [
      {label:"All",value:"all"},
      {label: "Less than Primary", value: "less_than_primary"},
      {label: "Primary School", value: "primary_school"},
      {label: "Secondary School", value: "secondary_school"},
      {label: "College", value: "college"},
      {label: "Graduate School", value: "graduate_school"}
    ],
    placeholder: "Please select Education level"
  };
const dataCountries = {
    items:[
        {
          "label": "Afghanistan",
          "value": "AF"
        },
        {
          "label": "Angola",
          "value": "AO"
        },
        {
          "label": "Argentina",
          "value": "AR"
        },
        {
          "label": "Australia",
          "value": "AU"
        },
        {
          "label": "Azerbaijan",
          "value": "AZ"
        },
        {
          "label": "Bangladesh",
          "value": "BD"
        },
        {
          "label": "Bolivia",
          "value": "BO"
        },
        {
          "label": "Brazil",
          "value": "BR"
        },
        {
          "label": "Canada",
          "value": "CA"
        },
        {
          "label": "Cote d'Ivoire",
          "value": "CI"
        },
        {
          "label": "Chile",
          "value": "CL"
        },
        {
          "label": "Cameroon",
          "value": "CM"
        },
        {
          "label": "Colombia",
          "value": "CO"
        },
        {
          "label": "Germany",
          "value": "DE"
        },
        {
          "label": "Algeria",
          "value": "DZ"
        },
        {
          "label": "Ecuador",
          "value": "EC"
        },
        {
          "label": "Estonia",
          "value": "EE"
        },
        {
          "label": "Egypt",
          "value": "EG"
        },
        {
          "label": "Spain",
          "value": "ES"
        },
        {
          "label": "France",
          "value": "FR"
        },
        {
          "label": "United Kingdom",
          "value": "GB"
        },
        {
          "label": "Georgia",
          "value": "GE"
        },
        {
          "label": "Ghana",
          "value": "GH"
        },
        {
          "label": "Guatemala",
          "value": "GT"
        },
        {
          "label": "Honduras",
          "value": "HN"
        },
        {
          "label": "Indonesia",
          "value": "ID"
        },
        {
          "label": "India",
          "value": "IN"
        },
        {
          "label": "Iraq",
          "value": "IQ"
        },
        {
          "label": "Italy",
          "value": "IT"
        },
        {
          "label": "Jamaica",
          "value": "JM"
        },
        {
          "label": "Japan",
          "value": "JP"
        },
        {
          "label": "Kenya",
          "value": "KE"
        },
        {
          "label": "Cambodia",
          "value": "KH"
        },
        {
          "label": "South Korea",
          "value": "KR"
        },
        {
          "label": "Kazakhstan",
          "value": "KZ"
        },
        {
          "label": "Sri Lanka",
          "value": "LK"
        },
        {
          "label": "Morocco",
          "value": "MA"
        },
        {
          "label": "Myanmar",
          "value": "MM"
        },
        {
          "label": "Mongolia",
          "value": "MN"
        },
        {
          "label": "Mexico",
          "value": "MX"
        },
        {
          "label": "Malaysia",
          "value": "MY"
        },
        {
          "label": "Mozambique",
          "value": "MZ"
        },
        {
          "label": "Nigeria",
          "value": "NG"
        },
        {
          "label": "Netherlands",
          "value": "NL"
        },
        {
          "label": "Nepal",
          "value": "NP"
        },
        {
          "label": "Peru",
          "value": "PE"
        },
        {
          "label": "Philippines",
          "value": "PH"
        },
        {
          "label": "Pakistan",
          "value": "PK"
        },
        {
          "label": "Poland",
          "value": "PL"
        },
        {
          "label": "Portugal",
          "value": "PT"
        },
        {
          "label": "Romania",
          "value": "RO"
        },
        {
          "label": "Sudan",
          "value": "SD"
        },
        {
          "label": "Singapore",
          "value": "SG"
        },
        {
          "label": "Senegal",
          "value": "SN"
        },
        {
          "label": "Thailand",
          "value": "TH"
        },
        {
          "label": "Turkey",
          "value": "TR"
        },
        {
          "label": "Trinidad & Tobago",
          "value": "TT"
        },
        {
          "label": "Taiwan",
          "value": "TW"
        },
        {
          "label": "Tanzania",
          "value": "TZ"
        },
        {
          "label": "Ukraine",
          "value": "UA"
        },
        {
          "label": "Uganda",
          "value": "UG"
        },
        {
          "label": "United Arab Emirates",
          "value": "AE"
        },
        {
          "label": "United States",
          "value": "US"
        },
        {
          "label": "Uruguay",
          "value": "UY"
        },
        {
          "label": "Venezuela",
          "value": "VE"
        },
        {
          "label": "Vietnam",
          "value": "VN"
        },
        {
          "label": "South Africa",
          "value": "ZA"
        }
      ],
      placeholder:"Please select the country"
};
const dataSignal = {
    items: [
        {
          "label": "Vaccine Acceptance",
          "value": "vaccine_accept"
        },
        {
          "label": "Vaccine Norms",
          "value": "norms_vaccine"
        },
        {
          "label": "Mismatch Index",
          "value": "mismatch_index"
        },
        {
          "label": "Community Risk Index",
          "value": "community_risk_index"
        },
        {
          "label": "Measures taken",
          "value": "measures_taken"
        },
        {
          "label": "Locations I would attend",
          "value": "locations_would_attend"
        },
        {
          "label": "Familiarity with the term 'physical distancing'",
          "value": "distancing_familiarity"
        },
        {
          "label": "Mask Effectiveness",
          "value": "effect_mask"
        },
        {
          "label": "News sources",
          "value": "news_sources"
        },
        {
          "label": "News mediums",
          "value": "news_mediums"
        },
        {
          "label": "Would visit a restaurant if...",
          "value": "restaurants"
        },
        {
          "label": "Would visit a retail store if...",
          "value": "retail"
        },
        {
          "label": "Would visit a health care center if...",
          "value": "health"
        },
        {
          "label": "Would visit a place of worship if...",
          "value": "worship"
        }
      ],
      placeholder: "Please Select the signal"
}
export {dataGroupAges, dataGender, dataEducation, dataCountries,dataSignal}