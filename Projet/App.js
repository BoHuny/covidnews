import React from 'react';
import { StyleSheet, Text, View, Button, LogBox } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';

import SondageComponent from './components/pages/SondageComponent';
import StatsMondeComponent from './components/pages/StatsMondeComponent';
import StatsPaysComponent from './components/pages/StatsPaysComponent';



export default function App() {
  LogBox.ignoreAllLogs();
  return (
    <NavigationContainer>
      <MaNavigation>
      </MaNavigation>
    </NavigationContainer>
  );
}

const Drawer = createDrawerNavigator();

const MaNavigation = () => {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name=" Welcome" component={StatsMondeComponent} />
      <Drawer.Screen name="News specific to a country" component={StatsPaysComponent} />
      <Drawer.Screen name="Surveys about covid" component={SondageComponent} />
    </Drawer.Navigator>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  }
});
